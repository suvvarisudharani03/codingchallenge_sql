--Database :- PetPals_The Pet Adoptiom Platform

------------------------------CREATING TABLES --------------------------------

--Table1: pets

Create Table Pets (
	PetID int Identity(1,1) Primary key not null ,
	Name varchar(50) not null ,
	Age int not null ,
	Breed varchar(50) not null ,
	Type varchar(50) not null,
	AvailableForAdoption  bit not null ,
) ;

--Table 2 : Shelters

Create Table Shelters (
	ShelterID int Identity(1,1) Primary key not null,
    Name varchar(50) not null,
    Location varchar(50) not null
);

--Table 3 : Donations 

Create Table  Donations (
    DonationID int Identity(1,1) Primary Key not null,
    DonorName varchar(50) not null,
    DonationType varchar(50) not null,
    DonationAmount decimal(10, 2) null, 
    DonationItem varchar(50) null,
    DonationDate Datetime not null
);

--Table 4 :- Adoption events 

Create Table AdoptionEvents (
    EventID int Identity(1,1) Primary Key not null,
    EventName varchar(50),
    EventDate DateTime,
    Location varchar(50)
);

--Table 5 :- Participants

Create Table Participants (
    ParticipantID int Identity(1,1) Primary Key not null,
    ParticipantName varchar(50),
    ParticipantType varchar(50),
    EventID int,
    Foreign Key (EventID) References AdoptionEvents(EventID)
);


create Table Users(UserId int primary Key , UserName varchar(50) not null , Email Varchar(50) not null , DOB Date );



-------------------ADDING / INSERTING VALUES INTO TABLES -------------------------

---INSERTING VALUES INTO PETS TABLE 

Insert into Pets (Name, Age, Breed, Type, AvailableForAdoption)
Values
('Buddy', 2, 'Labrador Retriever', 'Dog', 1),
('Whiskers', 1, 'Siamese', 'Cat', 1),
('Rocky', 3, 'German Shepherd', 'Dog', 0),
('Mittens', 2, 'Persian', 'Cat', 1),
('Max', 4, 'Golden Retriever', 'Dog', 1),
('Fluffy', 1, 'Ragdoll', 'Cat', 0),
('Charlie', 2, 'Poodle', 'Dog', 1),
('Oreo', 3, 'Domestic Shorthair', 'Cat', 1),
('Lucy', 1, 'Beagle', 'Dog', 0),
('Milo', 2, 'Maine Coon', 'Cat', 1);

----INSERTING VALUES INTO SHELTERS TABLE 

Insert Into Shelters (Name, Location)
Values
('Happy Paws Shelter', 'Beeramguda Street, Hitech City'),
('Safe Haven Animal Rescue', 'IDL lake Street, Moosapet'),
('Rescue Me Pet Center', 'GandiMaisamma, Dundigal'),
('Paws and Claws Sanctuary', 'Lane 123, Delhi'),
('Heavenly Tails Adoption Center', 'street 123, Hyderabad'),
('Whisker Wonderland Shelter', 'StreetLane 123, Chennai'),
('Furry Friends Haven', 'Center street, Uttarpradesh '),
('Sunshine Pet Rescue', 'Jhansi, UP'),
('Angel Paws Animal Shelter', 'Jaipur, Rajasthan'),
('Loving Hearts Pet Haven', 'Charminar Street, Hyderabad');

----INSERTING VALUES INTO DONATIONS TABLE 

Insert Into Donations (DonorName, DonationType, DonationAmount, DonationItem, DonationDate)
Values
('Swathi jain', 'Cash', 100.50, NULL, '2023-01-15 08:30:00'),
('Morya Shetty', 'Item', NULL, 'Pet Food', '2023-02-20 12:45:00'),
('Rashmi Jaiswal', 'Cash', 75.25, NULL, '2023-03-10 15:20:00'),
('Acharya bhatt', 'Item', NULL, 'Pet Toys', '2023-04-05 10:10:00'),
('Prachi', 'Cash', 50.75, NULL, '2023-05-22 14:00:00'),
('Aradhya sonee', 'Item', NULL, 'Blankets', '2023-06-18 09:45:00'),
('Shreyanshu Nayak', 'Cash', 120.00, NULL, '2023-07-03 11:30:00'),
('Aastha Jain', 'Item', NULL, 'Cat Litter', '2023-08-08 16:15:00'),
('Monalisa Nayak', 'Cash', 90.20, NULL, '2023-09-12 13:05:00'),
('Prakruthi Rose', 'Item', NULL, 'Dog Bowls', '2023-10-25 17:40:00');


---INSERTING VALUES INTO ADOPTION EVENTS 

Insert Into AdoptionEvents (EventName, EventDate, Location)
Values
('Pet Adoption Fair', '2023-01-28 14:00:00', 'Hyderabad'),
('Paws for Love', '2023-02-15 10:30:00', 'Uttarpradesh'),
('Furry Friends Festival', '2023-03-05 12:45:00', 'Uttarpradesh'),
('Adopt-a-Palooza', '2023-04-18 11:00:00', 'Moosapet'),
('Woofstock', '2023-05-09 15:20:00', 'Delhi'),
('Cats and Canines Carnival', '2023-06-22 09:45:00', 'Amphitheater'),
('Hearts and Paws Expo', '2023-07-14 13:30:00', 'Chennai'),
('Home for the Holidays', '2023-08-30 16:15:00', 'Uttarpradesh'),
('Rescue Me Rally', '2023-09-25 14:00:00', 'Rajasthan'),
('Purrfect Match Event', '2023-10-12 17:30:00', 'Dundigal');

---INSERTING VALUES INTO PARTICIPANTS 

INSERT INTO Participants (ParticipantName, ParticipantType, EventID)
VALUES
('Happy Paws Shelter', 'Shelter', 1),
('Loving Hearts Pet Haven', 'Shelter', 2),
('Manoj Hazari', 'Adopter', 1),
('Rakesh patil', 'Adopter', 3),
('Rescue Me Pet Center', 'Shelter', 4),
('Chandu Mishra', 'Adopter', 2),
('Furry Friends Haven', 'Shelter', 5),
('Nikhil Kanna', 'Adopter', 4),
('Paws and Claws Sanctuary', 'Shelter', 6),
('Vignesh Rishi', 'Adopter', 5);



--------------------------------QUERIES --------------------------------------------

---Query 1 :- Write an SQL query that retrieves a list of available pets (those marked as available for adoption)
--from the "Pets" table. Include the pet's name, age, breed, and type in the result set. Ensure that
--the query filters out pets that are not available for adoption


Select Name, Age, Breed, Type
FROM Pets
WHERE AvailableForAdoption = 1;


---Query 2 :- Write an SQL query that retrieves the names of participants (shelters and adopters) registered
--for a specific adoption event. Use a parameter to specify the event ID. Ensure that the query
--joins the necessary tables to retrieve the participant names and types.

Declare @EventID INT = 1;

Select Participants.ParticipantName, Participants.ParticipantType
From Participants
JOIN AdoptionEvents 
ON Participants.EventID = AdoptionEvents.EventID
WHERE Participants.EventID = @EventID;

---Query 3 :- Write an SQL query that calculates and retrieves the total donation amount for each shelter (by
--shelter name) from the "Donations" table. The result should include the shelter name and the
--total donation amount. Ensure that the query handles cases where a shelter has received no
--donations.


Alter Table Donations 
Add ShelterID int 

Select Shelters.Name as ShelterName, COALESCE(SUM(Donations.DonationAmount),0) as TotalDonationAmount
from Shelters
join 
Donations on Shelters.ShelterID = Donations.DonationID
Group by Shelters.Name ;

---QUERY 4 :- Write an SQL query that retrieves the total donation amount for each month and year (e.g.,
--January 2023) from the "Donations" table. The result should include the month-year and the
--corresponding total donation amount. Ensure that the query handles cases where no donations
--were made in a specific month-year

Select
Format(DonationDate, 'MMMM yyyy') as MonthYear,
COALESCE(SUM(DonationAmount), 0) AS TotalDonationAmount
from Donations
group by Format(DonationDate, 'MMMM yyyy');

----Query 5 :- Retrieve a list of distinct breeds for all pets that are either aged between 1 and 3 years or older
--than 5 years.


select Distinct Breed
from Pets
Where (Age BETWEEN 1 AND 3) OR (Age > 5);

---Query 6 :- Retrieve a list of pets and their respective shelters where the pets are currently available for
--adoption.

select Pets.PetID,Pets.Name as PetName,Pets.Breed,Pets.Type,Shelters.Name as ShelterName
from Pets
JOIN 
Participants ON Pets.PetID = Participants.ParticipantID
JOIN 
Shelters ON Participants.ParticipantID = Shelters.ShelterID
Where Pets.AvailableForAdoption = 1;

---Query 7:- Find the total number of participants in events organized by shelters located in specific city.
--Example: City=Chennai

DECLARE @City VARCHAR(255) = 'Chennai';

select COUNT(DISTINCT Participants.ParticipantID) as TotalParticipants
from Participants
JOIN AdoptionEvents ON Participants.EventID = AdoptionEvents.EventID
JOIN Shelters ON Participants.ParticipantID = Shelters.ShelterID
where Shelters.Location = @City;


--Query 8 :- Retrieve a list of unique breeds for pets with ages between 1 and 5 years.

select distinct Breed from Pets 
where Age BETWEEN 1 and 5;

--Query 9 :- . Find the pets that have not been adopted by selecting their information from the 'Pet' table.

select Pets.PetID,Pets.Name,Pets.Age,Pets.Breed,Pets.Type from pets 
where AvailableForAdoption = 1;


--Query 11 :- Retrieve a list of all shelters along with the count of pets currently available for adoption in each
--shelter.

select * from Shelters
select * from Pets
select * from Participants

select Shelters.ShelterID,Shelters.Name as ShelterName,COUNT(DISTINCT Pets.PetID) AS TotalPetsAvailable
from Shelters
Left join
Pets on Pets.ShelterID=Shelters.ShelterID
group by Shelters.ShelterID,Shelters.Name



---Query 12:- Find pairs of pets from the same shelter that have the same breed

Alter table Pets
Add ShelterID int;

Alter Table Pets
Add Constraint FK_Pets_Shelters
Foreign Key (ShelterID)
References Shelters(ShelterID);

select p1.PetID as Pet1ID,p1.Name as Pet1Name,p1.Breed as Breed,p2.PetID as Pet2ID,p2.Name as Pet2Name
FROM Pets p1 JOIN Pets p2 ON p1.ShelterID = p2.ShelterID
AND p1.Breed = p2.Breed
AND p1.PetID < p2.PetID

---Query 13 :- . List all possible combinations of shelters and adoption events

SELECT Shelters.ShelterID , Shelters.Name as ShelterName,AdoptionEvents.EventID,AdoptionEvents.EventName
FROM Shelters
CROSS JOIN AdoptionEvents

--Query 14: Retrieve the names of all adopted pets along with the adopter's name from the 'Adoption' and
--'User' tables

ALter table Pets
Add UserId int;

select Pets.Name as PetName , Users.UserName as AdopterName 
from Pets  
join Users 
on Pets.UserId = Users.UserId;


